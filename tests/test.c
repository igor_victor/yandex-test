#include <stdio.h>
#include "lib.h"

//check number of blocks
//and pointer step
void test_lock_block(unsigned int num_block)
{
  t_block* p_block;
  t_block* p_block_back;
  unsigned int count_blocks = 0;
  int different;
    
    //check pointer step
    if((p_block_back = lock_block()) != 0) count_blocks++;
    
    while( (count_blocks != num_block)
           &&((p_block = lock_block()) != 0) )
    {
      //check multiplicity size_block
      different = (unsigned int)p_block - (unsigned int)p_block_back;
      while(different > 0) {
        different -= size_block;
      }
      
      if(different == 0) { 
        p_block_back = p_block;
        count_blocks++;
      }
      else break;
    }
    
    //check number of blocks
    if(count_blocks == num_block) {
      printf("%s%d%s", "lock_block(", num_block, ") - Tested OK\r\n");
    }
    else if(p_block == 0) {
      printf("%s%d%s", "lock_block(", num_block, ") - Tested ERROR(pool is full)\r\n");
    }
    else if(different != 0) {
      printf("%s%d%s", "lock_block(", num_block, ") - Tested ERROR(pointer failure)\r\n");
    }
    else {
      printf("%s%d%s", "lock_block(", num_block, ") - Tested ERROR(indefinite)\r\n");      
    }
}

void test_free_block(unsigned int position, unsigned int num_block)
{
  unsigned char* p_block;
  
  p_block = (unsigned char*)get_head();
  
  //calculate pointer for position input
  for(unsigned int i=0; i<position; i++) {
    p_block += size_block;
  }
  //free blocks
  for(unsigned int i=0; i<num_block; i++) {
    free_block((t_block*)p_block);
    p_block += size_block;   
  }
  printf("%s%d,%d%s", "free_block(", position, num_block, ")\r\n");
}

void test_progress(void)
{
  printf("start test_progress\r\n");
  
  test_free_block(0, size_pool);
  test_lock_block(size_pool);
  
  for(unsigned int i=0; i<size_pool; i++) {
    test_free_block(0, i+1);
    test_lock_block(i+1);
  }
}

void test_regress(void)
{
  printf("start test_regress\r\n");
  
  test_free_block(0, size_pool);
  test_lock_block(size_pool);
  
  for(unsigned int i=size_pool; i>0; i--) {
    test_free_block(0, i);
    test_lock_block(i);
  }
}


