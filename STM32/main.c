#include "lib.h"
#include "test.h"


t_block* p_block;

int main()
{
  test_lock_block(1);//Tested ERROR(pointer failure) (uncomment p_bl++; in lock_block function)
  test_lock_block(size_pool+1);//Tested ERROR(pool is full)
  
  test_progress();
  test_regress();
  
  return 0;
}
