Размещение по каталогам:
src_block_alloc - модуль исходных кодов для аллокатора,
tests - модуль исходных кодов тестов аллокатора,
STM8 - проект для STM8
STM32 - проект для STM32 (без RTOS)
FreeRTOS - проект для STM32 с использованием FreeRTOS

Пояснения:
Для выполнения условий задачи были разработаны две функции:
t_block* lock_block(void) и void free_block(t_block*).
Работа данных функций на платформах различной разрядности и
в среде RTOS обеспечивается на уровне совместимости исходных кодов.
Размеры блока и пула задаются через дефайны.

Для проверки правильности решения были созданы три проекта.
Первый для STM8 в среде IAR Embedded Workbench for STM8 (v.3.10.4),
Второй для STM32 (без RTOS) в среде IAR Embedded Workbench for ARM (v.8.40.1),
Третий для STM32 (с использованием FreeRTOS) в среде STM32CubeMX (v.5.0.0) и
IAR Embedded Workbench for ARM (v.8.40.1)
Все эти проекты при компиляции используют исходные коды из одной папки (src_block_alloc)

В проектах для STM8 и STM32 (без RTOS) проводилось тестирование с использованием
написанных тестов. Вывод результатов тестирования производится в терминал.
Для проверки использовался встроенный в среду разработки эмулятор.

Проект для STM32 (с использованием FreeRTOS) просто демонстрирует возможность
работы модуля в многозадачной среде (был проверен на реальном железе STM32F407).
В данном проекте созданы три задачи:
первая запрашивает блок памяти каждые 500мС,
вторая запрашивает блок памяти каждые 700мС,
третья освобождает все занятые блоки памяти каждые 7 сек.
Взаимодействие между задачами организованно через очередь.
Вывод текстовых сообщений в терминал позволяет наблюдать за работой аллокатора.

