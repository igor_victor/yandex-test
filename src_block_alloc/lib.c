#include "lib.h"

static unsigned char  pool[size_pool * size_block];
static unsigned char stat_block[(size_pool >> 3) + 1];//status (bit field)



t_block* lock_block(void)
{  
  
#ifdef use_RTOS
  taskENTER_CRITICAL();
#endif
  
  unsigned char block_status;
  unsigned char *p_bl = pool;
  
  for(unsigned int i=0; i<size_pool; i++) {//scan bit-field
    block_status = stat_block[i >> 3] & (1 << (i & 0x7));
    if(block_status == 0) {
        //block free
        stat_block[i >> 3] |= (1 << (i & 0x7));//locked block
        
        #ifdef use_RTOS
          taskEXIT_CRITICAL();
        #endif
        return (t_block*)p_bl;//return pointer of block
    }
    p_bl += size_block;
    /* uncomment for tested "Tested ERROR(pointer failure)" */
    //p_bl++;
  }
  
  #ifdef use_RTOS
    taskEXIT_CRITICAL();
  #endif
  return 0;//all blocks used
}

void free_block(t_block* p_block)
{
#ifdef use_RTOS
  taskENTER_CRITICAL();
#endif
  
  unsigned char *p_block_current = pool;
  
  for(unsigned int i=0; i<size_pool; i++) {//scan bit-fields
    if(p_block == (t_block*)p_block_current) {
        stat_block[i >> 3] &= ~(1 << (i & 0x7));//unlocked block
        break;
    }
    p_block_current += size_block;
  }
  
#ifdef use_RTOS
  taskEXIT_CRITICAL();
#endif
}

//return pointer of head pool (for tests)
t_block* get_head(void)
{  
  return (t_block*)pool;
}




