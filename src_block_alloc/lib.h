/*uncomment for use with FreeRTOS*/
#define use_RTOS

#define size_block      (unsigned int)5//size of block
#define size_pool       (unsigned int)18//size of pool


#ifdef use_RTOS
  #include "FreeRTOS.h"
  #include "task.h"
#endif


typedef unsigned char *t_block;

t_block* lock_block(void);
void free_block(t_block*);
t_block* get_head(void);//for test


